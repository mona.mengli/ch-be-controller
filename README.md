# CH-BE controller

Controller software for CH-BE heating biasing chip

***
## CH-BE-V0.2.5(2018-10-01)


**Bug fixing:**

1. Fixed the PID tuning algorithm to solve the system unstable above a criticle temperature.
2. Fixed wrong wiring in blackdiagram the leads to incorrect PID value

**New function:**
1. Add PID value output in V0.2.5.1


